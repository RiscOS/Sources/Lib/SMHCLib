;
;  CDDL HEADER START
;
;  The contents of this file are subject to the terms of the
;  Common Development and Distribution License (the "Licence").
;  You may not use this file except in compliance with the Licence.
;
;  You can obtain a copy of the licence at
;  RiscOS/Sources/Lib/SMHCLib/LICENCE.
;  See the Licence for the specific language governing permissions
;  and limitations under the Licence.
;
;  When distributing Covered Code, include this CDDL HEADER in each
;  file and include the Licence file. If applicable, add the
;  following below this CDDL HEADER, with the fields enclosed by
;  brackets "[]" replaced with your own identifying information:
;  Portions Copyright [yyyy] [name of copyright owner]
;
;  CDDL HEADER END
;
;
;  Copyright 2021 RISC OS Developments Ltd.  All rights reserved.
;  Use is subject to license terms.
;

        EXPORT  CleanRange
        EXPORT  InvalidateRange


        AREA    |Asm$$Code|, CODE, READONLY


        ; Do a data cache clean to point of coherency of a range of addresses
; (Note this implementation assumes a Cortex-A53 and may not be applicable to other CPUs)
; On entry:
;   a1 -> first address to clean (inclusive)
;   a2 -> last address to clean (exclusive)
CleanRange ROUT
        ; Round outwards to next cacheline boundary
        ADD     a2, a2, #63
        BIC     a1, a1, #63
        BIC     a2, a2, #63
        ; Clean cachelines by MVA (logical address)
01      MCR     p15, 0, a1, c7, c10, 1 ; DCCMVAC
        ADD     a1, a1, #64
        CMP     a1, a2
        BNE     %BT01
        ; Now ensure cache maintenance operations before this point complete
        ; before any memory accesses by any observer (most importantly, the DMA
        ; controller) triggered by any instruction occurring after this point in
        ; program order
        DSB
        BX      lr


; Do a data cache invalidate to point of coherency of a range of addresses
; (Note this implementation assumes a Cortex-A53 and may not be applicable to other CPUs)
; On entry:
;   a1 -> first address to invalidate (inclusive)
;   a2 -> last address to invalidate (exclusive)
InvalidateRange ROUT
        ; Round outwards to next cacheline boundary
        ADD     a2, a2, #63
        BIC     a1, a1, #63
        BIC     a2, a2, #63
        ; Ensure that cache invalidation isn't observed until after reading the
        ; flag that indicated it was time to invalidate
        DMB
        ; Invalidate cachelines by MVA (logical address)
01      MCR     p15, 0, a1, c7, c6, 1 ; DCIMVAC
        ADD     a1, a1, #64
        CMP     a1, a2
        BNE     %BT01
        ; Examples in ARMv8 ARM K11.5.1 imply no need for a barrier afterwards
        BX      lr


        END
